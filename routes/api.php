<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {
    // Route::post('login', 'AuthController@login');
    Route::get('login', [ 'as' => 'login', 'uses' => 'AuthController@login']);
    Route::post('signup', 'AuthController@signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

// Route for get product list by seller

 Route::get('products/{id}', 'ProductController@getProductDetails');

// Route for get product list by seller

 Route::get('product/details/by/seller/{seller}', 'ProductController@getProductDetailsBySeller');


// Route for get seller details for a product

 Route::get('seller/details/for/products/{id}', 'ProductController@getSellerDetailsForAProduct');
