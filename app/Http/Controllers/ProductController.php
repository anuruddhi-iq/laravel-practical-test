<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use DB;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;

class ProductController extends BaseController
{
	public function __construct()

	{

		$this->middleware('auth:api');

	}


	// APIfor get product details
    public function getProductDetails(Request $request, $id)
    {
    	try{


	        $productDetails = DB::table('products')
			->join('seller_to_product_details', 'products.id', '=', 'seller_to_product_details.product_id')
			->join('sellers', 'seller_to_product_details.seller_id', '=', 'sellers.id')
			->select('products.id AS Product Id','products.name AS Product Name','products.description','products.image AS Product image URL','sellers.name AS Seller','seller_to_product_details.price','seller_to_product_details.stock')
			->where([
						['products.id','=',$id],
						['seller_to_product_details.status','=',Product::ACTIVE_PRODUCT]
					])
			->get();

			if(!$productDetails->isEmpty()){

				return response()->json([
					'code' => '200',
					'message' => 'get product details by id success',
					'data' => $productDetails
				]);
			}else{

				return response()->json([
					'code' => '404',
					'message' => 'get product details by id not found',
					'data' => $productDetails
				]);

			}

		}catch(\Exception $e) {

    		return $e->getMessage();
		}

    }



    // API for get product list by seller
    public function getProductDetailsBySeller($seller)
    {
    	try{

	        $productDetails = DB::table('sellers')
			->join('seller_to_product_details', 'sellers.id', '=', 'seller_to_product_details.seller_id')
			->join('products', 'seller_to_product_details.product_id', '=', 'products.id')
			 ->select('products.id AS Product Id','products.name AS Product Name','products.description','products.image AS Product image URL','seller_to_product_details.price','seller_to_product_details.stock')
			->where([
						['sellers.name','like', '%' .$seller . '%'],
						['seller_to_product_details.status','=',Product::ACTIVE_PRODUCT]
					])

			->get();

			if(!$productDetails->isEmpty()){
				
				return response()->json([
					'code' => '200',
					'message' => 'get product details by seller success',
					'data' => $productDetails
				]);
			}else{

				return response()->json([
					'code' => '404',
					'message' => 'get product details by seller not found',
					'data' => $productDetails
				]);

			}

		}catch(\Exception $e) {

    		return $e->getMessage();
		}

    }


    // API for get seller details for a product
    public function getSellerDetailsForAProduct($id)
    {
    	try{

	        $sellerDetails = DB::table('products')
			->join('seller_to_product_details', 'products.id', '=', 'seller_to_product_details.product_id')
			->join('sellers', 'seller_to_product_details.seller_id', '=', 'sellers.id')
			->select('sellers.name AS Seller','seller_to_product_details.price','products.id AS Product Id','products.name AS Product Name','products.description','seller_to_product_details.stock','products.image AS Product image URL')
			->where([
						['products.id','=',$id],
						['seller_to_product_details.status','=',Product::ACTIVE_PRODUCT]
					])
			->get();

			if(!$sellerDetails->isEmpty()){
				
				return response()->json([
					'code' => '200',
					'message' => 'get seller details for a product success',
					'data' => $sellerDetails,
				]);
			}else{

				return response()->json([
					'code' => '404',
					'message' => 'get seller details for a product not found',
					'data' => $sellerDetails
				]);

			}

    	}catch(\Exception $e) {

    		return $e->getMessage();
		}

	}
    
}
