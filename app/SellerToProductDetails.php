<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SellerToProductDetails extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'seller_id', 'stock', 'price', 'status',
    ];


}
