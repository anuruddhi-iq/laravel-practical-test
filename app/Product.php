<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	const ACTIVE_PRODUCT = '1';
	const DEACTIVE_PRODUCT = '0';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'description', 'image',
    ];

}
