-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 07, 2020 at 04:59 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2020_08_06_110042_create_product_table', 1),
(4, '2020_08_06_110352_create_seller_table', 1),
(5, '2020_08_06_110702_create_seller_to_product_details_table', 1),
(6, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(7, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(8, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(9, '2016_06_01_000004_create_oauth_clients_table', 2),
(10, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('39f842038da7a1c5387badf10943e65cbd4d002e22b79fb967327c80bffaa07275e4b49c88d7a007', 1, 3, 'Personal Access Token', '[]', 0, '2020-08-07 08:56:57', '2020-08-07 08:56:57', '2020-08-14 14:26:57'),
('49509c0610a84dcd1bb1a9632f5d566f8789b9ae953ca130201b8e618e0a673f0f9f5392646f1fd2', 1, 3, 'Personal Access Token', '[]', 0, '2020-08-07 05:02:36', '2020-08-07 05:02:36', '2020-08-14 10:32:36'),
('77b712cdb21f7c7b6923147b6c172437090f1e86dc0cea34f6bda6e0b78157601f95e05a3d4c5489', 1, 3, 'Personal Access Token', '[]', 0, '2020-08-07 06:12:24', '2020-08-07 06:12:24', '2020-08-14 11:42:24'),
('982e54b235dad6cf13b9b183c7af9593b492273e58c5f53e238b79087512bb4d23fc1448eac13438', 1, 3, 'Personal Access Token', '[]', 0, '2020-08-07 09:21:35', '2020-08-07 09:21:35', '2020-08-14 14:51:36'),
('cefe8f21daa0015370aba378ec3875762aa9b80f674372809f5015c2dc389c86a7e4c2ed9695a05e', 1, 3, 'Personal Access Token', '[]', 0, '2020-08-07 07:55:02', '2020-08-07 07:55:02', '2020-08-14 13:25:03'),
('d44506d7de0ed8289c63a11865f18b25944d2ad52a3a0e21488bcea55f5d85b3a53b01b3256e47ca', 1, 3, 'Personal Access Token', '[]', 0, '2020-08-07 05:16:16', '2020-08-07 05:16:16', '2020-08-14 10:46:17'),
('f35aab8f93785e02b8870b6f227b0d77e56323d7ce6d23861174b81a9ee8980e1e26f905bf591c53', 1, 3, 'Personal Access Token', '[]', 0, '2020-08-07 00:22:49', '2020-08-07 00:22:49', '2020-08-14 05:52:50');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'uzfArW8oqDAvJ3sC5lA3seGbaeyMcAEctkYqiPsA', NULL, 'http://localhost', 1, 0, 0, '2020-08-06 23:03:53', '2020-08-06 23:03:53'),
(2, NULL, 'Laravel Password Grant Client', 'Fr3P0kl7h8n3o1mNvLNdkIRPfIoRke2xTjdZ1n9w', 'users', 'http://localhost', 0, 1, 0, '2020-08-06 23:03:53', '2020-08-06 23:03:53'),
(3, NULL, 'Laravel Personal Access Client', 'fpFBbHM8O3q8C7Ex8nuRgoNuq0ztW2DpyFrcFZsh', NULL, 'http://localhost', 1, 0, 0, '2020-08-06 23:04:12', '2020-08-06 23:04:12'),
(4, NULL, 'Laravel Password Grant Client', 'WxUf17wkKtWAe3GvN83JiC5JAFjnLJCPpgatWXMO', 'users', 'http://localhost', 0, 1, 0, '2020-08-06 23:04:12', '2020-08-06 23:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-08-06 23:03:53', '2020-08-06 23:03:53'),
(2, 3, '2020-08-06 23:04:12', '2020-08-06 23:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `description`, `image`) VALUES
(1, 'Singer Vista 32 HD Android Smart TV', 'Brand: Singer,Type: Vista HD Android Smart TV,Model: SLE32F590,Size: 32', 'http://127.0.0.1:8000/storage/tv.jpg'),
(2, 'Lenovo Laptop', 'Lenovo IP340 Celeron Dual Core 4GB Ram, 1TB HDD, WIN10', 'http://127.0.0.1:8000/storage/laptop.jpg'),
(3, 'SAMSUNG GALAXY A70', 'Technology GSM / HSPA / LTE, 2G bands GSM 850 / 900 / 1800 / 1900 - SIM 1 & 			  SIM 2 (dual-SIM model only)', 'http://127.0.0.1:8000/storage/mobile.jpg'),
(4, 'JBL Free X Earphone- Black', 'Discover the freedom of a wireless lifestyle listening to music, managing 				your calls or working-out.', 'http://127.0.0.1:8000/storage/headset.jpg'),
(5, 'Samsung LED TV Full HD 40', 'Type	FULL HD,Screen Size	40, Display Resolution	1,920 x 1,080, Viewing Angle Backlight Type	LED', 'http://127.0.0.1:8000/storage/samsungtv.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `sellers`
--

CREATE TABLE `sellers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sellers`
--

INSERT INTO `sellers` (`id`, `name`, `description`) VALUES
(1, 'Singer', 'Excellent Customer Service provided by Singer Sri lanka.. And the variety of brands are exciting'),
(2, 'Abans', 'shopping stores which provides best price laptops, ac, mobile phones, washing machine, led tv for flexible rates in Sri Lanka'),
(3, 'Dialcom', 'All Mobile Phones, Tabs, I pads Best Performer Agent price in sri lanka');

-- --------------------------------------------------------

--
-- Table structure for table `seller_to_product_details`
--

CREATE TABLE `seller_to_product_details` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `seller_id` int(10) UNSIGNED NOT NULL,
  `stock` int(11) NOT NULL,
  `price` decimal(8,2) NOT NULL,
  `status` tinyint(4) NOT NULL COMMENT 'Active - 1,Deactive - 0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seller_to_product_details`
--

INSERT INTO `seller_to_product_details` (`id`, `product_id`, `seller_id`, `stock`, `price`, `status`) VALUES
(1, 1, 1, 10, '49000.00', 1),
(2, 2, 2, 25, '84500.00', 1),
(3, 2, 1, 15, '85000.00', 1),
(4, 3, 3, 17, '53000.00', 1),
(5, 4, 3, 36, '2100.00', 1),
(6, 5, 1, 7, '49800.00', 1),
(7, 3, 1, 27, '54600.00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Navodya', 'wanniarachchi091@gmail.com', NULL, '$2y$10$.l5ejvwZYrcQ9EDy4DoSa.WAquNF9NVi2PZ5W2FY3XYQ7shL7fp0W', NULL, '2020-08-07 00:14:08', '2020-08-07 00:14:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sellers`
--
ALTER TABLE `sellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seller_to_product_details`
--
ALTER TABLE `seller_to_product_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `sellers`
--
ALTER TABLE `sellers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `seller_to_product_details`
--
ALTER TABLE `seller_to_product_details`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
