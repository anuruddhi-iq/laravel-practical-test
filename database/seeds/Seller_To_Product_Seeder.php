<?php

use Illuminate\Database\Seeder;

class Seller_To_Product_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('seller_to_product_details')->insert([[
            'product_id' =>"1" ,
            'seller_id' =>"1" ,
            'stock' =>"10" ,
            'price' =>"49000.00" ,
            'status' =>"1" ,
        ],
    [
            'product_id' =>"2" ,
            'seller_id' =>"2" ,
            'stock' =>"25" ,
            'price' =>"84500.00" ,
            'status' =>"1" ,
        ],
    [
            'product_id' =>"2" ,
            'seller_id' =>"1" ,
            'stock' =>"15" ,
            'price' =>"85000.00" ,
            'status' =>"1" ,
        ],
    [
            'product_id' =>"3" ,
            'seller_id' =>"3" ,
            'stock' =>"17" ,
            'price' =>"53000.00" ,
            'status' =>"1" ,
        ],
    [
            'product_id' =>"4" ,
            'seller_id' =>"3" ,
            'stock' =>"36" ,
            'price' =>"2100.00" ,
            'status' =>"1" ,
        ],
     
 [
            'product_id' =>"5" ,
            'seller_id' =>"1" ,
            'stock' =>"7" ,
            'price' =>"49800.00" ,
            'status' =>"1" ,
        ],
    [
            'product_id' =>"3" ,
            'seller_id' =>"1" ,
            'stock' =>"27" ,
            'price' =>"54600.00" ,
            'status' =>"1" ,
        ]]);
    }
    
}
