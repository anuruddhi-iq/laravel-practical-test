<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('products')->insert([[
            'name' =>"Singer Vista 32 HD Android Smart TV" ,
            'description' =>"Brand: Singer,Type: Vista HD Android Smart TV,Model: SLE32F590,Size: 32" ,
            'image' =>"http://127.0.0.1:8000/storage/tv.jpg" ,
        ],
    [
            'name' =>"Lenovo Laptop" ,
            'description' =>"Lenovo IP340 Celeron Dual Core 4GB Ram, 1TB HDD, WIN10" ,
            'image' =>"http://127.0.0.1:8000/storage/laptop.jpg" ,
        ],
    [
            'name' =>"SAMSUNG GALAXY A70" ,
            'description' =>"Technology GSM / HSPA / LTE, 2G bands GSM 850 / 900 / 1800 / 1900 - SIM 1 & 			  SIM 2 (dual-SIM model only)" ,
            'image' =>"http://127.0.0.1:8000/storage/mobile.jpg" ,
        ],
    [
            'name' =>"JBL Free X Earphone- Black" ,
            'description' =>"Discover the freedom of a wireless lifestyle listening to music, managing 				your calls or working-out." ,
            'image' =>"http://127.0.0.1:8000/storage/headset.jpg" ,
        ],
    [
            'name' =>"Samsung LED TV Full HD 40" ,
            'description' =>"Type	FULL HD,Screen Size	40, Display Resolution	1,920 x 1,080, Viewing Angle Backlight Type	LED" ,
            'image' =>"http://127.0.0.1:8000/storage/samsungtv.jpg" ,
        ]]);
    }
}
