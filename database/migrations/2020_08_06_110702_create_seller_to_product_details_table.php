<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSellerToProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seller_to_product_details', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_id')->nullable(false);
            $table->unsignedInteger('seller_id')->nullable(false);
            $table->integer('stock')->nullable(false);
            $table->decimal('price',8,2)->nullable(false);
            $table->tinyInteger('status')->comment('Active - 1,Deactive - 0')->nullable(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seller_to_product_details');
    }
}
